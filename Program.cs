﻿using System;

namespace task_24
{
// Person
    class Person
    {
        string Name;
        int Age;
        string Address02;
        string Address{ get; set; }
        public string GetAddress
        {
            get {
                    return Address02;
                }
        }
        public string GetAddress02
        {
            get{
                return Address02;
               }
        }
        public string SetAddress
        {
            set {
                Address02 = value;
                }
        }
        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }
        public void Sayhello()
        {
            Console.WriteLine("Hello World");
            Console.WriteLine($"Hi my name is {Name} and I am {Age} years old");
        }

    }

    





        class Program
    {
       
        static void Main(string[] args)
        {
           var person01 = new Person("Joe", 47);
           person01.Sayhello();

           person01.SetAddress = "200 Cameron Road";
           Console.WriteLine(person01.GetAddress);

           var person02 =new Person("bob", 47);
           person02.Sayhello();
           person02.SetAddress = "201 Cameron Road";
        }
    }
}
